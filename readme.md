## Raspberry Pi Fan Monitor

Raspberry Pi has a build-in temperature reading. This is a simple project to monitor the temperature and turn on the mini fan if it is over a definable threshold, off it when it is below, by doing this, we bring down the power consumption even lower.
 
### Hardware 
1. `BC457` npn Transistpr
2. `1K ohm` resister
3. `Raspberry Pi Cooling Fan` 5V FZ0732: 3V - 5.8V, 0.35W, 1m3/h

### Software 
- git clone [WiringPi](git://git.drogon.net/wiringPi)
- git clone [PiFanMonitor](https://bitbucket.org/kkgian/pifanmon)

### Methology
- Using cron job 
- Read Pi temperature and compare threshold
- Swtich on fan if above, off if below

#### Credits:
http://wiringpi.com


